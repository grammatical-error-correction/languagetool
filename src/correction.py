import language_tool_python

class Correction():
    def __init__(self, sentences, api):
        self.source = sentences
        self.output = []
        self.tool = language_tool_python.LanguageToolPublicAPI('en-US') if api else language_tool_python.LanguageTool('en-US')
        self.json = {}
    
    def get_correction(self):
        for sent in self.source:
            self.output.append(self.tool.correct(sent))
    
    # def define_api_false(self, if_api):
    #     if if_api == 'False':
    #         self.api = False
    #         self.tool = language_tool_python.LanguageTool('en-US')