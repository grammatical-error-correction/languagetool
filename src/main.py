import argparse

from src.correction import Correction

def correct(sentences, api, save):
    correction = Correction(sentences, api)
    correction.get_correction()
    if save == 'False' or save == False:
        for x, y in zip(args.sentence, correction.output):
            print("Original: ", x)
            print("Corrected: ", y)
            print(' ')
    else:
        with open('results.txt', 'w') as f:
            f.writelines(correction.output)
            f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Choose between sentence or batch correction')
    parser.add_argument('--sentence', nargs='+', help='Enter the sentence you want to correct')
    parser.add_argument('--batch', nargs='+', help='Enter the path of the batch you want to correct')
    parser.add_argument('--api', help='True to use LT API, False to use LT locally', default=True)
    parser.add_argument('--save', help='True to save the results in a txt file', default=False)
    args = parser.parse_args()

    if args.sentence != None:
        correct(args.sentence, args.api, args.save)
    
    if args.batch != None:
        for batch in args.batch:
            with open(batch) as f:
                sentences = f.read().splitlines()
                correct(sentences, args.api, args.save)