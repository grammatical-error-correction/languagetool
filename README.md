# LanguageTool API

> An easy, fast and free way to use LanguageTool API

This repository provides an easy way to use the API of LanguageTool for Grammatical Error Correction (FREE VERSION) as well as the local LT version issued from the Python package language_tool_python. Thanks to LanguageTool, it is now amazingly simple to correct sentences and batches with low effort and tremendous results.

# Table of contents

- [LanguageTool](#languagetool)
- [Installation](#installation)
- [Quick Start](#quick-start)
- [Docker](#docker)
- [References](#references)

# LanguageTool
LanguageTool is an Open Source proofreading software for English, French, German, Polish, Russian, and more than 20 other languages. It finds many errors that a simple spell checker cannot detect.

- [Jobs at LanguageTool](https://languagetool.org/careers)
- [How to run your own LanguageTool server](https://dev.languagetool.org/http-server)
- [HTTP API documentation](https://languagetool.org/http-api/swagger-ui/#!/default/post_check)
- [How to use LanguageTool's public server via HTTP](https://dev.languagetool.org/public-http-api)
- [How to use LanguageTool from Java](https://dev.languagetool.org/java-api) (Javadoc)

For more information, please see LanguageTool's homepage at [https://languagetool.org](https://languagetool.org/), [this README](https://github.com/languagetool-org/languagetool/blob/master/languagetool-standalone/README.md), and [CHANGES](https://github.com/languagetool-org/languagetool/blob/master/languagetool-standalone/CHANGES.md).

LanguageTool is freely available under the LGPL 2.1 or later.

For this project, LanguageTool offer a simple HTTPS REST-style service that anybody can use to check texts with LanguageTool. The only public endpoint is the following one - do not send your requests to any other endpoints you might find in the homepage’s HTML code or elsewhere -:

[https://api.languagetool.org/v2/check](https://api.languagetool.org/v2/check)

When using it, please keep the following rules in mind:

- Do not send automated requests. For that, set up your own instance of LanguageTool or get an account for Enterprise use.
- Only send POST requests, not GET requests.
- Access is currently limited to:

> 20 requests per IP per minute (this is supposed to be a peak value - don’t constantly send this many requests or LanguageTool would have to block you)

> 75KB text per IP per minute

> 20KB text per request

> Only up to 30 misspelled words will have suggestions

This is a free service, thus there are no guarantees about performance or availability. The limits may change anytime.

The LanguageTool version installed may be the latest official release or some snapshot. LanguageTool will simply deploy new versions, thus the behavior will change without any warning.

# Installation

After cloning the project,

```
pip3 install requirements.txt
```

Export the project to your PYTHONPATH.

# Quick start

With LanguageTool, you can use either correct sentences or batches.

```
usage: main.py [-h] [--sentence SENTENCE [SENTENCE ...]] [--batch BATCH [BATCH ...]] [--api API] [--save SAVE]

Choose between sentence or batch correction

optional arguments:
  -h, --help            show this help message and exit
  --sentence SENTENCE [SENTENCE ...]
                        Enter the sentence(s) you want to correct
  --batch BATCH [BATCH ...]
                        Enter the path of the batch you want to correct
  --api API             True to use LT API, False to use LT locally, default is True
  --save SAVE           True to save the results in a txt file, default is False
```

-------

  
# Docker

It's now a possibility to use Docker to run LanguageTool.

Follow these steps in order to build:
- `docker build . -t language_tool --network=host  # it will build the docker image`
  
Then you can do whatever you want (from correcting sentences to batches, and returning the full JSON):
Example:
```
- docker run language_tool:latest --sentence 'I have many dog.' 'I want a orange.'
- docker run language_tool:latest --sentence 'I have many dog.' --save True
- docker run language_tool:latest --sentence 'I have many dog.' --api False
```

# References

- [LanguageTool](https://languagetool.org/)
- [LanguageToolPython](https://github.com/jxmorris12/language_tool_python)
- [How to run your own LanguageTool server](https://dev.languagetool.org/http-server)
- [HTTP API documentation](https://languagetool.org/http-api/swagger-ui/#!/default/post_check)
- [How to use LanguageTool's public server via HTTP](https://dev.languagetool.org/public-http-api)